from django.contrib import admin
from django.urls import path
# importamos agora as classes diretamente
from enquete.views import ListaEnquete, DetalheEnquete

urlpatterns = [
    path('admin/', admin.site.urls),
    path("enquetes/", ListaEnquete.as_view(), name="lista_enquete"),
    path("enquetes/<int:pk>/", DetalheEnquete.as_view(), name="detalhe_enquete"),
]
