from rest_framework import serializers

from .models import Pergunta, Alternativa


class AlternativaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alternativa
        fields = '__all__'


class PerguntaSerializer(serializers.ModelSerializer):
    alternativa = AlternativaSerializer(many=True, read_only=True, required=False)
    class Meta:
        model = Pergunta
        fields = '__all__'
