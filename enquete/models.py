from django.db import models

class Pergunta(models.Model):
    enunciado = models.CharField(max_length=200)
    data_publicacao = models.DateTimeField('data de publicação')

    def __str__(self):
        return self.enunciado

    def publicado_recentemente(self):
        return self.data_publicacao >= timezone.now() - datetime.timedelta(days=1)


class Alternativa(models.Model):
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    alternativa_texto = models.CharField(max_length=200)
    escolha = models.IntegerField(default=0)

    def __str__(self):
        return self.alternativa_texto
