from django.contrib import admin

# Register your models here.

from .models import Pergunta, Alternativa

admin.site.register(Pergunta)

admin.site.register(Alternativa)
