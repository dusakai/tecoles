from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .models import Pergunta, Alternativa
from .serializers import PerguntaSerializer


MAX_OBJECTS = 20


class ListaEnquete(APIView):

    def get(self, request):
        enquetes = Pergunta.objects.all()[:MAX_OBJECTS]
        dado = PerguntaSerializer(enquetes, many=True).data
        return Response(dado)


class DetalheEnquete(APIView):

    def get(self, request, pk):
        enquete = get_object_or_404(Pergunta, pk=pk)
        dado = PerguntaSerializer(enquete).data
        return Response(dado)
